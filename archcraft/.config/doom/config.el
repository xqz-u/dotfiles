;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "xqz-u"
      user-mail-address "marco.gallo0530@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font
      (font-spec :family "MesloLGS NF" :size 20 :weight 'semi-light))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-gruvbox
      ;; doom-monokai-classic-brighter-comments nil
      )

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.


;; ---------------------------------------------------------------------------

;; TODO function that, when hitting C-RET in emacs-lisp mode,
;; opens a ielm buffer in a new window if one does not exist yet,
;; and at each call evals expression as point in the buffer
;; TODO `find-file' that shows directories and symlinks highlighted,
;; as in spacemacs NOTE probably needs `helm'
;; TODO remove some ligatures
;; TODO integrate missing packages

;; add private modes
(add-load-path! "private")
;; load my functions
(load! "private/funcs.el")


;; global keybindings
(map!
 (:leader ;; prexix the following commands with SPC

  ;; set original keybinding for SPC b B under SPC b b
  ;; "b B" nil
  :desc "Switch buffer [remapped]"
  "b b" #'counsel-switch-buffer

  :desc "Comment line from point"
  "C" #'comment-line

  :desc "Switch to previous buffer"
  "TAB" #'evil-switch-to-windows-last-buffer

  :desc "Execute..."
  "SPC" #'counsel-M-x

  :desc "Kill sexp at point"
  "d s" #'kill-sexp

  ;; as in spacemacs
  :desc "evil-window-vsplit"
  "w /" #'evil-window-vsplit)

 :ng "C-+" nil
 :desc "text scale +"
 "C-+" #'text-scale-increase

 :ng "C-=" nil
 :desc "text scale reset"
 "C-=" #'doom/reset-font-size)


(setq doom-modeline-enable-word-count t ;; show words in selected region
      confirm-kill-emacs nil ;; exit emacs directly
      )

;; set mouse pointer color to yellowish, visible both on dark and light themes
;; NOTE check frame properties by calling `(frame-parameters)'
(add-to-list 'default-frame-alist
             '(mouse-color . "goldenrod3"))

;; correct approach for setting faces
;; HACK
;; for this one specifically the cursor
;; needs to change color and be overridden by the sp face, rn colors are
;; mismatched
(custom-set-faces!
  '(sp-show-pair-match-face
    :weight normal
    :inverse-video t
    :foreground "#A6E22E"
    :background "#1B2229"))

;; ligatures stuff
(plist-put! +ligatures-extra-symbols
            :return-hint ""
            :leq "≤"
            :geq "≥")

;; FIXME does not work :/
(after! prog-mode
  (set-ligatures! 'prog-mode
    :leq "<="
    :geq ">="))

;; auto-fill every text mode
(add-hook 'text-mode-hook #'auto-fill-mode)

;; to use mouse scrolling in DocView mode
(setq doc-view-continuous t)

;; TODO org syntax for citations does not work yet...
;; default useful latex packages in org-mode
;; TODO re-render pdf on org-latex-compile, rn visiting the buffer is needed
;; TODO synctex stuff to move between pdf and source, if it exists?
(setq org-latex-packages-alist '(("" "apacite")
                                 ("" "natbib")
                                 ("" "cleveref")
                                 ;; ("newfloat" "minted") TODO add for minted
                                 ))


;; to use paredit bindings
;; see https://github.com/hlissner/doom-emacs/issues/4374 as to why
;; evil-smartparens is needed
(use-package! smartparens
  :init (add-hook 'smartparens-enabled-hook #'evil-smartparens-mode)
  :config (sp-use-paredit-bindings)
  :hook ((prog-mode . show-smartparens-mode)
         (python-mode . smartparens-strict-mode)
         (emacs-lisp-mode . smartparens-strict-mode)))

(use-package! format-all
  :hook (prog-mode . format-all-mode))

(use-package! rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package! python
  :init
  (add-hook 'before-save-hook #'python-cleanup-hooks)
  :config
  (setq python-shell-interpreter-args "" ;; default: -i
        )
  (set-ligatures! 'python-mode
    ;; :tuple nil FIXME does not work...
    :return-hint "->"))

(use-package! elpy
  :config
  (push 'elpy-project-find-local-root elpy-project-root-finder-functions)
  (map! :after python
        :map python-mode-map
        (:localleader
         :prefix "e"
         :desc "eval buffer"
         "b" #'elpy-shell-send-buffer-and-step)
        (:desc "eval statement"
         [remap +default/newline-below] #'elpy-shell-send-statement-and-step)
        (:localleader
         :desc "open REPL"
         "r" #'elpy-shell-get-or-create-process)))

;; (use-package! poetry
;;   :config
;;   (setq poetry-tracking-strategy 'projectile))

;; spotify
;; NOTE some keybindings don't work out of the box, like that for smudge-track-add
;; and smudge-recently-played
;; (use-package! smudge
;;   :config
;;   (setq smudge-oauth2-client-id "db1c6c4d3b5c44f4a6b3edb6a59dfe72"
;;         smudge-oauth2-client-secret "0bac41c74dcc4b409b3e24ede582cc77"
;;         smudge-player-status-refresh-interval 10 ;; default 5
;;         smudge-player-status-format "[%a, %t ◷ %l]")
;;   (map!
;;    :map smudge-mode-map
;;    :desc "smudge commands"
;;    "C-c ." 'smudge-command-map
;; 
;;    :map smudge-command-map
;;    :desc "show player status"
;;    "s" #'toggle-smudge-player-indicator
;; 
;;    :desc "add current song to playlist"
;;    "p a" #'smudge-add-current-track-to-playlist)
;;   (setq smudge-player-status-format "[%a, %t %l]")
;;   (global-smudge-remote-mode +1))
