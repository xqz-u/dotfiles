(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#272822" "#E74C3C" "#A6E22E" "#E6DB74" "#268bd2" "#F92660" "#66D9EF" "#F8F8F2"])
 '(custom-safe-themes
   '("7eea50883f10e5c6ad6f81e153c640b3a288cd8dc1d26e4696f7d40f754cc703" default))
 '(exwm-floating-border-color "#3c3d38")
 '(fci-rule-color "#555556")
 '(highlight-tail-colors ((("#333a23") . 0) (("#2d3936") . 20)))
 '(jdee-db-active-breakpoint-face-colors (cons "#1B2229" "#FD971F"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1B2229" "#A6E22E"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1B2229" "#525254"))
 '(objed-cursor-color "#E74C3C")
 '(pdf-view-midnight-colors (cons "#F8F8F2" "#272822"))
 '(rustic-ansi-faces
   ["#272822" "#E74C3C" "#A6E22E" "#E6DB74" "#268bd2" "#F92660" "#66D9EF" "#F8F8F2"])
 '(vc-annotate-background "#272822")
 '(vc-annotate-color-map
   (list
    (cons 20 "#A6E22E")
    (cons 40 "#bbdf45")
    (cons 60 "#d0dd5c")
    (cons 80 "#E6DB74")
    (cons 100 "#edc457")
    (cons 120 "#f5ad3b")
    (cons 140 "#FD971F")
    (cons 160 "#fb7134")
    (cons 180 "#fa4b4a")
    (cons 200 "#F92660")
    (cons 220 "#f33254")
    (cons 240 "#ed3f48")
    (cons 260 "#E74C3C")
    (cons 280 "#c14d41")
    (cons 300 "#9c4f48")
    (cons 320 "#77504e")
    (cons 340 "#555556")
    (cons 360 "#555556")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(sp-show-pair-match-face ((t (:weight normal :inverse-video t :foreground "#A6E22E" :background "#1B2229")))))
