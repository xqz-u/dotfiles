;;; ../../dotfiles/system_conf/.config/doom/private/funcs.el -*- lexical-binding: t; -*-

;; >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
;; python

;; NOTE relies on format-all-mode to be present and enabled, could simply
;; install python-black.el for finer control
;; TODO error checking, e.g. there is a py-isort-before-save fn which does so...
(defun python-cleanup-hooks ()
  "In python mode, runs isort and black on save"
  (when (eq major-mode 'python-mode)
    (py-isort-buffer)
    (+format-buffer-h)))

(defun elpy-project-find-local-root ()
  "Set elpy's project root to directory where .elpy-root file is located."
  (locate-dominating-file default-directory ".elpy-root"))

;; >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
;; smudge spotify client

;; TODO show info about song & playlist it was added to in success message
;; TODO if song was already in selectd playlist, notify user and fail
(defun smudge-add-current-track-to-playlist ()
  "Adds the currently playing song to a playlist chosen interactively"
  (interactive)
  (smudge-track-select-playlist
   (lambda (playlist)
     (smudge-api-current-user
      (lambda (user)
        (smudge-api-get-player-status
         (lambda (status)
           (let ((track (gethash 'item status)))
             (smudge-api-playlist-add-track
              (smudge-api-get-item-id user)
              playlist
              (smudge-api-get-item-uri track)
              (lambda (_)
                (message "Current song added to playlist.")))))))))))


;; TODO ideally, toggle refreshing the indicator status when this function is called
;; tried with smudge-controller-stop-player-status-timer, but (smudge-controller-timerp)
;; still returns true...
(defun toggle-smudge-player-indicator ()
  "Toggles show of smudge currently playing song in the modeline"
  (interactive)
  (let ((s `(,smudge-title-bar-separator (:eval (smudge-remote-player-status-text)))))
    (if (member s global-mode-string)
        (setq global-mode-string (remove s global-mode-string))
      (push s global-mode-string))))


;; >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
;; hash table
;;

;; TODO test
;; (defun hash-table-alist (ht)
;;   (let ((acc nil))
;;     (maphash #'(lambda (k v)
;;                  (if (hash-table-p v)
;;                      (push (cons k (hash-table-alist v)) acc)
;;                    (push (cons k v) acc)))
;;              ht)
;;     acc))

;; TODO test
;; (defun hash-table-keys (hash-table)
;;   "Collects the keys of a hash table in a list"
;;   (let ((keys ()))
;;     (maphash (lambda (k v)
;;                (if (hash-table-p v)
;;                    (push (hash-table-keys v) keys)
;;                  (push k keys)))
;;              hash-table)
;;     keys))
