;;; ../../dotfiles/system_conf/.config/doom/private/gin-mode.el -*- lexical-binding: t; -*-

(add-to-list 'auto-mode-alist '("\\.gin" . gin-mode))

;;;###autoload
(define-derived-mode gin-mode python-mode "Gin"
  "Major mode for syntax-highlighting .gin files,
starting by the font locks defined in python-mode")

(provide 'gin-font-lock)

