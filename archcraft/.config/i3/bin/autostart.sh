#!/usr/bin/env bash

## Autostart Programs

# Kill already running process
_ps=(compton dunst ksuperkey mpd polybar xfce-polkit xfce4-power-manager)
for _prs in "${_ps[@]}"; do
	if [[ `pidof ${_prs}` ]]; then
		killall -9 ${_prs}
	fi
done

# polkit agent
/usr/lib/xfce-polkit/xfce-polkit &

# Enable power management
xfce4-power-manager &

# Enable Super Keys For Menu
ksuperkey -e 'Super_L=Alt_L|F1' &
ksuperkey -e 'Super_R=Alt_L|F1' &

# Restore wallpaper
hsetroot -cover ~/.config/i3/wallpapers/default.png
# hsetroot -cover ~/.config/i3/wallpapers/corey.jpg

# Lauch notification daemon
~/.config/i3/bin/i3dunst.sh

# Lauch polybar
~/.config/i3/bin/i3bar.sh

# Lauch compositor
~/.config/i3/bin/i3comp.sh

# Start mpd
# exec mpd &

# Adjust backlight (AMD)
#blight -d amdgpu_bl0 set 15%

# ====================================================

# launch `main` polybar on all detected displays
# ~/.config/i3/bin/detect_screens_polybar.sh

# Start MechVibes
# ~/executables/Mechvibes-2.3.0.AppImage &

# tell rofi launcher to use my custom keymap
setxkbmap -layout us -variant dvp

# Shift + Caps Lock is the regular Caps Lock
setxkbmap -option caps:escape_shifted_capslock

# start geolocation agent to use redshift
/usr/lib/geoclue-2.0/demos/agent &
