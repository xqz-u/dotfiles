#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# create log file
[ ! -f ~/.polybar_logs ] & touch ~/.polybar_logs

# Launch polybar on all connected monitors and log stderr to file
for m in $(polybar --list-monitors | cut -d":" -f1); do
    MONITOR=$m polybar main -c ~/.config/i3/polybar/config.ini 2>>~/.polybar_logs &
done



# NOTE polybar can be restarted in the following ways:
# 1. polybar-msg cmd restart
# 2. by launching a bar with the --reload option, then any chenges to the
# config.ini file will cause a reload
