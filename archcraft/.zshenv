export PATH=$HOME/scripts:$HOME/executables:$HOME/usr/local/bin:$HOME/.local/bin:$HOME/.emacs.d/bin:$PATH

# what ssh stuff was this for?
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"


# virtualenvwrapper
export WORKON_HOME="$HOME/py_envs"
source "$HOME/.local/bin/virtualenvwrapper.sh"

# source nvm stuff here so that emacs can find them, does not happen if in .zshrc
source "/usr/share/nvm/init-nvm.sh"

# cardano related
# compilation
export LD_LIBRARY_PATH="/usr/local/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/usr/local/lib/pkgconfig:$PKG_CONFIG_PATH"
# running
# export CARDANO_NODE_SOCKET_PATH="$HOME/cardano/testnet/db/node.socket"
