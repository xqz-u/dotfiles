# for i3-sensible-terminal
export TERMINAL=alacritty

export PATH="$HOME/scripts:$HOME/.local/bin:$HOME/go/bin:$PATH"

export WORKON_HOME="$HOME/py_envs"

# what ssh stuff was this for?
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

# https://stackoverflow.com/questions/47893138/back-space-not-functional-in-python-and-ipython-in-shell
# export TERMINFO=/usr/share/terminfo

# for proper configuration of KDE applications, e.g. icons in Dolphin file manager
export QT_QPA_PLATFORMTHEME="qt5ct"

# for Language Server Protocol efficiency in Emacs
# https://emacs-lsp.github.io/lsp-mode/page/performance/#use-plists-for-deserialization
export LSP_USE_PLISTS=true

# source nvm stuff here so that emacs can find them, does not happen if in .zshrc
source "/usr/share/nvm/init-nvm.sh"

# >>> mamba initialize >>>
# !! Contents within this block are managed by 'mamba init' !!
export MAMBA_EXE="/home/xqz-u/.local/bin/micromamba";
export MAMBA_ROOT_PREFIX="/home/xqz-u";
__mamba_setup="$('/home/xqz-u/.local/bin/micromamba' shell hook --shell zsh --prefix '/home/xqz-u/micromamba' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__mamba_setup"
else
    if [ -f "/home/xqz-u/micromamba/etc/profile.d/micromamba.sh" ]; then
	. "/home/xqz-u/micromamba/etc/profile.d/micromamba.sh"
    else
	export  PATH="/home/xqz-u/micromamba/bin:$PATH"  # extra space after export prevents interference from conda init
    fi
fi
unset __mamba_setup
# <<< mamba initialize <<<

# cardano related
# compilation
# export LD_LIBRARY_PATH="/usr/local/lib:$LD_LIBRARY_PATH"
# export PKG_CONFIG_PATH="/usr/local/lib/pkgconfig:$PKG_CONFIG_PATH"
# running
# export CARDANO_NODE_SOCKET_PATH="$HOME/cardano/testnet/db/node.socket"
