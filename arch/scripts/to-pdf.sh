#!/usr/bin/sh

# TODO convert all .jpg etc in a folder, ask user confirmation
# (integrate with confirmation function from `tex-emacs-setup.sh`)

# $1: new file name
# the remaining positional parameters are the sources of $1
convert ${@:2} $1
