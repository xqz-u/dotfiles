#! /usr/bin/zsh

CARDANO_HOME=$HOME/cardano/testnet

cardano-wallet serve \
               --node-socket $CARDANO_HOME/db/node.socket \
               --database $CARDANO_HOME/db/wallets \
               --listen-address 0.0.0.0 \
               --testnet $CARDANO_HOME/config/testnet-byron-genesis.json
