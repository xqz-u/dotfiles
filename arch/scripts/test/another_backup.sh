#!/usr/bin/zsh
setopt extendedglob # zsh version of shopt -s extglob (bash)

# This function performs a FULL BACKUP IN CURRENT FOLDER
# TODO enable it as cl arg
backup_current() {
    backup_dir=$(basename "$(pwd)")_BACKUP_$(date +%d-%m-%y-%H.%M)
    mkdir "$backup_dir"
    # [[ -d "$backup_dir" ]] \
        #     && echo "Backup dir: $backup_dir" \
        #         || echo "WARNING $backup_dir not created!"
    cp -r ./*~("./$backup_dir"|"$this_file") "$backup_dir"
    # ls "$backup_dir"
    backup_arch="$backup_dir.tar.gz"
    # echo "Archive: $backup_arch"
    tar -cpzf "$backup_arch" "$backup_dir"
    rm -rf "$backup_dir"
}

this_file="$0"
# backup_current
