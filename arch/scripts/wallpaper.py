#!/usr/bin/python

import os
import random
import re
import subprocess as sp

allowed_img_ext = [".png", ".jpg", ".jpeg"]
allowed_img_ext += list(map(str.upper, allowed_img_ext))


def images_matching_resolution(folder: str, res: str) -> list:
    """
    Returns files in FOLDER which contain the substring RES
    (a resolution string returned by `xrandr`, e.g. 1920x1080).
    File extension are checked against global parameter
    ALLOWED_IMG_EXT.
    """
    return [
        f"{folder}/{f}"
        for f in os.listdir(folder)
        if os.path.splitext(f)[1] in allowed_img_ext and res in f
    ]


def connected_monitors_res() -> list:
    """
    Returns a list of tuples (monitor name, monitor resolution)
    for the connected monitors according to `xrandr`.
    """
    xrandr = sp.check_output(["xrandr"]).decode()
    return re.findall(r"(.+) connected [a-zA-Z]* *(\d+x\d+)", xrandr)


def randomize_wallpapers(pics_dir: str, monitors: list) -> list:
    """
    Chooses a random desktop background image from PICS_DIR for each screen
    in MONITORS with proper resolution. Does nothing if no suitable image
    (resolution wise) is found for a screen. NOTE images are set for monitors
    in the screen order returned by `xrandr`.
    """
    bgs = []
    for mon, res in monitors:
        matching_ims = images_matching_resolution(pics_dir, res)
        if not matching_ims:
            print(f"No image matching resolution {res} for screen {mon} in {pics_dir}")
            continue
        rand_wp = random.choice(matching_ims)
        bgs.append(rand_wp)
        print(f"Chose {rand_wp} as wallpaper for screen {mon}")
    return bgs


def main(pics_home: str) -> int:
    monitors = connected_monitors_res()
    print(f"Connected monitors: {monitors}")
    bgs = randomize_wallpapers(pics_home, monitors)
    if not bgs:
        print("No suitable images in target folder! Exit")
        return 1
    status = sp.run(["feh", "--bg-center", *bgs])
    print("Done")
    return status.returncode


# TODO allow usage of pictures which do not match full screen resolution
if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        pics_home = sys.argv[1]
    else:
        pics_home = "/home/xqz-u/pics"
        print(f"No folder to search images given, using default: {pics_home}")
    exit(main(pics_home))
