# adapted from https://gist.github.com/mvetsch/9bfdee7c56140eae2baa0be711fc8ded

# $1: network interface name
# $2: eduroam username

nmcli connection add type wifi con-name eduroam ifname $1 ssid "eduroam" -- \
      wifi-sec.key-mgmt wpa-eap 802-1x.eap ttls 802-1x.identity $2 \
      802-1x.phase2-auth mschapv2 802-1x.domain-suffix-match radius.rug.nl

# activate with
nmcli connection up eduroam --ask
