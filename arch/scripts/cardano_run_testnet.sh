#! /usr/bin/zsh

CARDANO_HOME=$HOME/cardano/testnet

cardano-node run \
             --config $CARDANO_HOME/config/testnet-config.json \
             --database-path $CARDANO_HOME/db/ \
             --socket-path $CARDANO_HOME/db/node.socket \
             --host-addr 127.0.0.1 \
             --port 1337 \
             --topology $CARDANO_HOME/config/testnet-topology.json
