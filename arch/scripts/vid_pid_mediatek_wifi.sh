#!/bin/bash

# Insert vendor and product id of ComFast CF-953AX (driver mt7921u) into some
# array?
# VID and PID can be seen with `lsusb`
echo 3574 6211 > /sys/bus/usb/drivers/mt7921u/new_id
