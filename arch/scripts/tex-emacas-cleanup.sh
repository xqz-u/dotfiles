#!/usr/bin/sh

# some extensions and filenames to cleanup
RM=( .log .aux .synctex.gz .fdb_latexmk \
          .fls .toc .out _region_.tex auto \
          _minted-* .bbl
   )

# input directory where dump files are located
DIR=$1

EXPANDED=()
for f in ${RM[@]/#/$DIR/*}; do
    if [[ -e $f || -d $f ]]; then
        EXPANDED+=( $f )
    fi
done

# show which files will be removed and ask for confirmation
echo "wanna remove ${EXPANDED[@]}? (y/n)"
read ans
case $ans in
    [Yy]* ) rm -r ${EXPANDED[@]} 2>/dev/null;
            break;;
    [Nn]* ) exit;;
    * ) echo "please answer y/n";;
esac
