#!/bin/bash

# Simple script to perform a backup on an predefined external drive (atm)
# TODO schedule cronjob over SSH!

SOURCE=~/Desktop
EXT_DRIVE=/run/media/pasta/SAMSUNG
DRIVE_LOGS=$EXT_DRIVE/backup_logs
BACKUP_NAME=BACKUP_$(basename $SOURCE)_DIR

[[ -e $EXT_DRIVE ]] || {
    echo "Failed backup on $EXT_DRIVE, check if mounted!" && exit 1
}

[[ -d $DRIVE_LOGS ]] || mkdir -p $DRIVE_LOGS

start_ext=$(date +%s)

sudo rsync -azvin --delete --exclude-from="exclude_list.txt" \
     --ignore-missing-args $SOURCE $EXT_DRIVE/$BACKUP_NAME \
     1>$DRIVE_LOGS/"$BACKUP_NAME"_$(date +%m%d%Y.%H-%M-%S).log \
     2>$DRIVE_LOGS/"$BACKUP_NAME"_$(date +%m%d%Y.%H-%M-%S).err

end_ext=$(date +%s)

notify-send "Finished job $BACKUP_NAME! $EXT_DRIVE runtime: $((end_ext-start_ext))s"
