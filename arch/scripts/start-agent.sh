#!/usr/bin/sh

if [ $(ps ax | grep --regexp "[s]sh-agent" | wc -l) -le 0 ]; then
    eval $(ssh-agent -s)
    ssh-add
    echo "=== Agent started ==="
else
    echo "The agent is already up!"
fi
