#! /usr/bin/env bash

# exactly match $1 in the remaining arguments $@:1
match_word_in() {
    search=$1
    shift 1
    echo $@ | grep -q -P "\b$search\b"
}

# collect functions declaration of a script - excluding this one and passed args
# $@ - and return as string
available_cmds() {
    ret=()
    excludes=($@ ${FUNCNAME[0]})
    for fn in $(compgen -A function); do
        if ! match_word_in $fn ${excludes[@]}; then
            ret+=($fn)
        fi
    done
    echo ${ret[@]}
}

# execute functions given as args, incrementally store their result and return
# it. check that functions exist in `available_cmds`, excluding `execute_cmds` to
# avoid infinite recursion
execute_cmds() {
    ret_arr=()
    for cmd in $@; do
        if match_word_in $cmd $(available_cmds ${FUNCNAME[0]}); then
            echo "Executing '$cmd'..."
            ret_arr+=( $(eval $cmd) )
        else
            echo "Unrecognised command \"$cmd\""
            ret_arr=""
            break
        fi
    done
    echo ${ret_arr[@]}
}
