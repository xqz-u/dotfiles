# first conda activate <env_name>
pip install jupyterlab \
    black \
    isort \
    jupyterlab-vim \
    jupyterlab-theme-solarized-dark \
    jupyterlab-code-formatter \
    nbdev
