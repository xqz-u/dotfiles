#! /usr/bin/zsh

CARDANO_HOME=$HOME/cardano/mainnet

cardano-node run \
             --config $CARDANO_HOME/config/mainnet-config.json \
             --database-path $CARDANO_HOME/db/ \
             --socket-path $CARDANO_HOME/db/node.socket \
             --host-addr 127.0.0.1 \
             --port 1337 \
             --topology $CARDANO_HOME/config/mainnet-topology.json
