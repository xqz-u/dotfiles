# NOTE requires the (relative) path to the notebook you want to clean...

import sys

from nbdev.clean import nbdev_clean

nb_path = sys.argv[1]
print(f"will clean nb {nb_path}")

nbdev_clean(nb_path)

print("DONE!")
